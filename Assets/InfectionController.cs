using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InfectionController : MonoBehaviour
{
    public int InfectedPeople = 0;
    public int RecoveredPeople = 0;
    public float updateTimer = 0;
    public TMP_Text infectados;
    public TMP_Text recuperados;

    void Start()
    {

    }

    public void addInfectedPeople()
    {
        InfectedPeople++;
    }

    public void addRecoveredPeople()
    {
        RecoveredPeople++;
    }

    void Update()
    {
        updateTimer += Time.deltaTime;
        if (updateTimer >= 1) // Se actualiza cada 60 segundos
        {
            infectados.text = InfectedPeople.ToString();
            recuperados.text = RecoveredPeople.ToString();      
            Debug.Log("There are " + InfectedPeople + " infected people");
            Debug.Log("There are " + RecoveredPeople + " recovered people");
            updateTimer = 0;
        }
    }
}




