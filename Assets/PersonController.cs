
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonController : MonoBehaviour
{
    private InfectionController infectionController;
    private float infectionDuration = 40f; // Duraci�n de la infecci�n en segundos
    private float infectionRadius = 2f; // Radio de infecci�n en unidades de distancia
    public enum ActualState
    {
        Susceptible,
        Infected,
        Recovered
    }
    public ActualState state;
    private bool isInfected = false;

    void Start()
    {
        infectionController = FindObjectOfType<InfectionController>();

        int random = Random.Range(0, 2);
        if (random == 1)
        {
            GetInfected();
        }
    }

    void GetInfected()
    {
        if (state != ActualState.Susceptible || isInfected)
        {
            return;
        }
        state = ActualState.Infected;
        StartCoroutine(SpreadInfection());
        StartCoroutine(RecoverAfterDuration()); // Se recuperar� despu�s de la duraci�n de la infecci�n
        infectionController.addInfectedPeople();
        isInfected = true;
    }

    IEnumerator SpreadInfection()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, infectionRadius);
        foreach (Collider collider in colliders)
        {
            PersonController person = collider.GetComponent<PersonController>();
            if (person != null && person.state == ActualState.Susceptible)
            {
                person.GetInfected();
            }
        }
        yield return null;
    }

    IEnumerator RecoverAfterDuration()
    {
        yield return new WaitForSeconds(infectionDuration);
        state = ActualState.Recovered;
        infectionController.addRecoveredPeople();
    }
}






