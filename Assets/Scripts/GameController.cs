using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public GameObject avatarPrefab;
    public GameObject[] classrooms;
    public GameObject[] entrances;
    public GameObject mainHall;
    public GameObject center;
    public TMP_InputField inputField;
    public TMP_Text myText;

    private int maxSpawns;
    private string input;
    //Declaraci�n de la zona de aparici�n

    private float spawnX = 138f;
    private float spawnY = 1f;
    private float spawnZmin = -22f;
    private float spawnZmax = 22f;

    private int spawnCounter;
    private float spawnDelay = .12f; //En segundos
    private float timeElapsed;


    void Start()
    {
        ObtenerNumeroDeSpawns("70");
        inputField = GetComponent<TMP_InputField>();
        timeElapsed = 0;
        spawnCounter = 0;
    }
    public string getCadena()
    {
        input = myText.text;
        return input;
    }
   
    public void ObtenerNumeroDeSpawns(string texto)
    {
        if (int.TryParse(texto, out int resultado))
        {
            Debug.Log("TODO OK");
            maxSpawns = resultado;
        }
        else
        {
            Debug.LogError("El texto ingresado no es un n�mero v�lido.");
        }
    }



    void Update()//Generaci�n de personas hasta el n�mero estableicdo
        {
            if (spawnCounter < maxSpawns)
            {
                timeElapsed += Time.deltaTime;
                if (timeElapsed > spawnDelay)
                {
                    timeElapsed -= spawnDelay;
                    spawnCounter++;
                    SpawnAvatar();
                    if (spawnCounter == maxSpawns)
                    {
                        Debug.Log("Finished spawning");
                    }
                }
            }
        }
    

    private void SpawnAvatar()//Genera personas de forma aleatoria en el rango establecido
    {
        Vector3 spawnPoint = new Vector3(spawnX, spawnY, Random.Range(spawnZmin, spawnZmax));
        GameObject instAvatar = Instantiate(avatarPrefab, spawnPoint, avatarPrefab.transform.rotation) as GameObject;
    }

}
