using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class MovementController : MonoBehaviour
{
    //Se establece lo puntos m�xmimos de movimiento
    private float xMin = -20f;
    private float xMax = 120f;
    private float y = 1f;
    private float zMin = -22f;
    private float zMax = 22f;

    private NavMeshAgent agent;
    private Vector3 startPosition;
    private int moveCycles = 0;

    private bool isMoving = false;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        startPosition = transform.position;
        StartCoroutine(StartMovement());
    }

    IEnumerator StartMovement()//ciclos de movimiento
    {
        while (true)
        {
            int cyclesLimit = Random.Range(5, 8); // N�mero aleatorio de ciclos
            moveCycles = 0;

            while (moveCycles < cyclesLimit)
            {
                MoveToRandomPoint();
                isMoving = true;
                yield return new WaitForSeconds(Random.Range(20f, 30f)); // Tiempo de movimiento m�s largo
                ReturnToStart();
                isMoving = false;
                moveCycles++;
                yield return new WaitForSeconds(2f); // Pausa de 3 segundos entre ciclos
            }

            if (!isMoving)
            {
                transform.position = startPosition; // Regresar al punto de partida
                yield return new WaitForSeconds(Random.Range(2f, 5f)); // Espera antes de reaparecer
            }
        }
    }

    private void MoveToRandomPoint()//establece un punto aleatorio al que se mueve el personaje
    {
        Vector3 randomPoint = GetRandomPointToMove();
        agent.SetDestination(randomPoint);
    }

    private void ReturnToStart()//el personaje vuelve al punto de inicio
    {
        agent.SetDestination(startPosition);
    }

    private Vector3 GetRandomPointToMove()//asegura que se mueve a una posici�n v�lida y comienza el movimineto
    {
        Vector3 randomPoint;
        NavMeshHit hit;
        bool found = false;
        do
        {
            randomPoint = new Vector3(Random.Range(xMin, xMax), y, Random.Range(zMin, zMax));
            found = NavMesh.SamplePosition(randomPoint, out hit, .5f, 1);
        } while (!found);

        return hit.position;
    }
}

